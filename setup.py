from setuptools import find_packages, setup
from pathlib import Path

directory = Path(__file__).parent

setup(
    name="pkgbuild",
    version="0.1.0",
    packages=find_packages(),
    entry_points={
        "console_scripts": [f"pkgbuild=pkgbuild.main:main"],
    },
    author="Pursuit",
    author_email="fr.pursuit@gmail.com",
    description="Package building tool",
    long_description=(directory / "README.md").read_text(encoding="utf-8"),
    long_description_content_type="text/markdown",
    url="https://gitlab.com/frPursuit/pkgbuild",
    license="GNU General Public License v3 (GPLv3)",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    ],
    python_requires=">=3.8"
)
