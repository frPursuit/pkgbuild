from typing import List

from pkgbuild.packages.packageref import PackageRef


class PackageSpec:
    @staticmethod
    def is_generic_architecture(architecture: str) -> bool:
        return architecture in ["any", "anycpu", "all", "noarch"]

    def __init__(self, name: str, program_version: str, package_revision: int, architecture: str,
                 maintainer: str, description: str):
        self.name = name
        self.program_version = program_version
        self.package_revision = package_revision
        self.architecture = architecture
        self.maintainer = maintainer
        self.description = description
        self.dependencies: List[PackageRef] = []

    def get_name(self) -> str:
        return self.name

    def get_program_version(self) -> str:
        return self.program_version

    def get_package_revision(self) -> int:
        return self.package_revision

    def get_package_version(self) -> str:
        return f"{self.program_version}-{self.package_revision}"

    def get_architecture(self) -> str:
        return self.architecture

    def is_architecture_generic(self) -> bool:
        return PackageSpec.is_generic_architecture(self.get_architecture())

    def get_maintainer(self) -> str:
        return self.maintainer

    def get_description(self) -> str:
        return self.description

    def get_file_name(self) -> str:
        return f"{self.get_name()}_{self.get_package_version()}"

    def add_dependency(self, ref: PackageRef):
        self.dependencies.append(ref)

    def get_dependencies(self) -> List[PackageRef]:
        return self.dependencies

    def __str__(self):
        return self.get_file_name()
