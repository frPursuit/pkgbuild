import os.path
import shutil
from tempfile import mkdtemp
from typing import Optional

from pkgbuild.packages.packagespec import PackageSpec
from pkgbuild.projects.project import Project


class Package:
    TYPE_NAME = "unknown"

    def __init__(self, project: Project, revision: int):
        self.project = project
        self.revision = revision

    def build(self, output: Optional[str] = None):
        temp_dir = mkdtemp(prefix="pkgbuild-")
        try:
            spec = self.project.get_package_spec(self.revision)
            package_root = os.path.join(temp_dir, spec.get_file_name())
            os.mkdir(package_root)

            print("Building project...")
            self.project.build(package_root)

            print(f"Building {type(self).TYPE_NAME} package...")
            self.build_directory(spec, package_root, output)
        finally:
            shutil.rmtree(temp_dir)

    def build_directory(self, spec: PackageSpec, directory: str, output: Optional[str]):
        raise NotImplementedError()
