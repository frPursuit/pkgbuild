class PackageRef:
    VALID_FILTERS = [">", "<", "==", ">=", "<="]

    def __init__(self, name: str, version_filter: str, version: str):
        """
        :param name: The name of the package
        :param version_filter: Either ">", "<", "==", ">=" or "<="
        :param version: The version of the package
        """
        if version_filter not in PackageRef.VALID_FILTERS:
            raise ValueError(f"Invalid version filter: {version_filter}")
        self.name = name
        self.version_filter = version_filter
        self.version = version

    def get_name(self) -> str:
        return self.name

    def get_version_filter(self) -> str:
        return self.version_filter

    def get_version(self) -> str:
        return self.version

    def __str__(self) -> str:
        return f"{self.name}{self.version_filter}{self.version}"
