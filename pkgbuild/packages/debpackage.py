import os.path
import shutil
from typing import Optional

from pkgbuild import utils
from pkgbuild.packages.package import Package
from pkgbuild.packages.packagespec import PackageSpec


class DebPackage(Package):
    TYPE_NAME = "deb"
    SPEC_FILE_PATH = "DEBIAN/control"

    def build_directory(self, spec: PackageSpec, directory: str, output: Optional[str]):
        if output is None:
            output = f"{spec.get_file_name()}.deb"

        self.generate_spec_file(spec, directory)
        utils.run("dpkg-deb", "--build", directory)
        result = f"{directory}.deb"
        shutil.move(result, output)

    def generate_spec_file(self, spec: PackageSpec, directory: str):
        path = os.path.join(directory, DebPackage.SPEC_FILE_PATH)
        parent_dir = os.path.dirname(path)
        os.makedirs(parent_dir, exist_ok=True)

        with open(path, "w") as spec_file:
            spec_file.write(f"Package: {spec.get_name()}\n")
            spec_file.write(f"Version: {spec.get_package_version()}\n")
            spec_file.write(f"Section: base\n")
            spec_file.write(f"Priority: optional\n")

            arch = spec.get_architecture() if not spec.is_architecture_generic() else "all"
            spec_file.write(f"Architecture: {arch}\n")

            dependencies = spec.get_dependencies()
            if len(dependencies) > 0:
                spec_file.write("Depends: " + ", ".join([f"{ref.get_name()} ({ref.get_version_filter()} {ref.get_version()})"
                                                         for ref in dependencies]) + "\n")

            spec_file.write(f"Maintainer: {spec.get_maintainer()}\n")
            spec_file.write(f"Description: " + spec.get_description().replace("\n", "\n ") + "\n")
