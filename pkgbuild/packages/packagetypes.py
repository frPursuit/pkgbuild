from typing import List, Type

from pkgbuild.packages.debpackage import DebPackage
from pkgbuild.packages.package import Package


class PackageTypes:
    DEB = DebPackage

    @staticmethod
    def get_all() -> List[Type[Package]]:
        types = []
        for field_name in dir(PackageTypes):
            item = getattr(PackageTypes, field_name)
            # Ignore functions and internal fields
            if field_name.startswith("_") or not isinstance(item, type):
                continue
            types.append(item)
        # noinspection PyTypeChecker
        return types

    @staticmethod
    def get_all_names() -> List[str]:
        return [ptype.TYPE_NAME for ptype in PackageTypes.get_all()]

    @staticmethod
    def get(name: str) -> Type[Package]:
        for ptype in PackageTypes.get_all():
            if ptype.TYPE_NAME == name:
                return ptype
        raise KeyError(f"Unknown package type: {name}")
