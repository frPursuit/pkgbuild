import argparse
import sys
import traceback

import pkgbuild
from pkgbuild.packages.packagetypes import PackageTypes
from pkgbuild.projects.projecttypes import ProjectTypes

# Exit error codes
RUNTIME_EXCEPTION = 10


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("-v", "--verbose", action="store_true", help="Increase the output's detail level")
    parser.add_argument("-V", "--version", help="display the program's version", action="version", version=f"Package builder {pkgbuild.__version__} - by Pursuit")
    parser.add_argument("pkg-type", choices=PackageTypes.get_all_names(), help="The type of package to build")
    parser.add_argument("project", nargs='?', help="The project to build")
    parser.add_argument("-r", "--revision", default="1", help="The package's revision. Defaults to 1")
    parser.add_argument("-t", "--project-type", choices=ProjectTypes.get_all_names(), required=False, help="The project's type")
    parser.add_argument("-o", "--output", required=False, help="The file to save the package as")

    args = parser.parse_args()

    try:
        package_type = PackageTypes.get(getattr(args, "pkg-type"))
        project_dir = args.project or "."
        revision = int(args.revision)
        if args.project_type is not None:
            project_type = ProjectTypes.get(args.project_type)
        else: project_type = ProjectTypes.detect_project_type(project_dir)
        output = args.output

        project = project_type(project_dir)
        package = package_type(project, revision)
        package.build(output)
    except Exception as e:
        sys.stderr.write(f"{e}\n")
        if args.verbose:
            traceback.print_exc()
        exit(RUNTIME_EXCEPTION)


if __name__ == "__main__":
    main()
