import subprocess
from typing import Optional


def run(*command: str, working_dir: Optional[str] = None):
    if working_dir is not None:
        result = subprocess.run(command, cwd=working_dir)
    else: result = subprocess.run(command)

    if result.returncode != 0:
        raise ChildProcessError(f"Unable to execute command (return code {result.returncode}): {command_to_string(*command)}")


def command_to_string(*command: str) -> str:
    formatted = []
    for part in command:
        if " " in part:
            formatted.append(f"\"{part}\"")
        else: formatted.append(part)
    return " ".join(formatted)
