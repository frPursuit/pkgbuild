from pkgbuild.packages.packagespec import PackageSpec


class Project:
    TYPE_NAME = "unknown"

    @classmethod
    def is_project_of_type(cls, directory: str) -> bool:
        return False

    def __init__(self, directory: str):
        self.directory = directory

    def get_directory(self) -> str:
        return self.directory

    def build(self, directory: str):
        raise NotImplementedError()

    def get_package_spec(self, revision: int) -> PackageSpec:
        raise NotImplementedError()
