import os.path
import re
import shutil
from pathlib import Path

from pkgbuild import utils
from pkgbuild.packages.packageref import PackageRef
from pkgbuild.packages.packagespec import PackageSpec
from pkgbuild.projects.project import Project


class PythonProject(Project):
    TYPE_NAME = "python"
    SETUP_FILE = "setup.py"
    SYSTEM_PACKAGES = "usr/lib/python3/dist-packages"
    PYTHON_NAME = "python3"
    PIP_NAME = "pip3"

    # Most python 3 libraries available as system packages have names in the "python3-<package>"
    # format where <package> is the name of the pypi package, but some of them have other names
    # that are described in this map
    PACKAGE_NAMES = {
        "PyYAML": "python3-yaml"
    }

    @classmethod
    def is_project_of_type(cls, directory: str) -> bool:
        return os.path.isfile(os.path.join(directory, PythonProject.SETUP_FILE))

    def build(self, directory: str):
        # Build Python package
        setup_info = self.read_setup_info()
        package_name = setup_info["name"]
        package_version = setup_info["version"]
        utils.run(PythonProject.PYTHON_NAME, "-m", "build", working_dir=self.directory)
        python_build = os.path.join(self.directory, "dist", f"{package_name}-{package_version}.tar.gz")

        # Install Python package in a tmp directory
        tmp_dir = os.path.join(directory, "tmp")
        os.mkdir(tmp_dir)
        utils.run(PythonProject.PIP_NAME, "install", "--no-dependencies", python_build, "--target", tmp_dir)

        # Move the installed files at the appropriate locations
        bin_dir = os.path.join(tmp_dir, "bin")
        if os.path.isdir(bin_dir):
            shutil.move(bin_dir, os.path.join(directory, "bin"))
        packages_dir = os.path.join(directory, PythonProject.SYSTEM_PACKAGES)
        os.makedirs(os.path.dirname(packages_dir))
        shutil.move(tmp_dir, packages_dir)

    def get_package_spec(self, revision: int) -> PackageSpec:
        setup_info = self.read_setup_info()
        python_name = setup_info["name"]
        version = setup_info["version"]
        author = setup_info["author"]
        author_email = setup_info["author_email"]
        description = setup_info["description"]

        # Using a standard name in the "python3-<package>" format for this package too
        spec = PackageSpec(self.get_package_name(python_name), version, revision, "any", f"{author} <{author_email}>", description)
        if "install_requires" in setup_info:
            for dep in setup_info["install_requires"]:
                spec.add_dependency(self.parse_dependency(dep))
        return spec

    def read_setup_info(self) -> dict:
        setup_path = os.path.join(self.directory, PythonProject.SETUP_FILE)
        with open(setup_path, "r") as setup_file:
            setup_script = setup_file.read()
        matches = re.findall(r"setup\((?P<info>.*)\)", setup_script, flags=re.DOTALL)
        if len(matches) != 1:
            raise IOError("Unable to accurately read setup info from setup.py")
        return eval(f"dict({matches[0]})", self.create_setup_context())

    def parse_dependency(self, dep: str) -> PackageRef:
        filter_name = None
        filter_index = None

        for flt in PackageRef.VALID_FILTERS:
            index = dep.find(flt)
            # Get the biggest possible filter present in the dependency definition
            if index >= 0 and (filter_name is None or len(filter_name) < len(flt)):
                filter_name = flt
                filter_index = index

        if filter_name is None:
            return PackageRef(dep, ">=", "0.0.0")
        else:
            name = dep[:filter_index]
            package_name = self.get_package_name(name)
            version = dep[filter_index+len(filter_name):]
            return PackageRef(package_name, filter_name, version)

    def get_package_name(self, python_name: str):
        if python_name in PythonProject.PACKAGE_NAMES:
            return PythonProject.PACKAGE_NAMES[python_name]
        lower_name = python_name.lower()
        return f"python3-{lower_name}"

    def create_setup_context(self) -> dict:
        # Create a context equivalent to the one available in setup.py
        return {
            "directory": Path(self.directory),
            "find_packages": lambda *args, **kwargs: "",
        }
