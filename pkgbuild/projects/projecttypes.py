from typing import List, Type

from pkgbuild.projects.project import Project
from pkgbuild.projects.pythonproject import PythonProject


class ProjectTypes:
    PYTHON = PythonProject

    @staticmethod
    def get_all() -> List[Type[Project]]:
        types = []
        for field_name in dir(ProjectTypes):
            item = getattr(ProjectTypes, field_name)
            # Ignore functions and internal fields
            if field_name.startswith("_") or not isinstance(item, type):
                continue
            types.append(item)
        # noinspection PyTypeChecker
        return types

    @staticmethod
    def get_all_names() -> List[str]:
        return [ptype.TYPE_NAME for ptype in ProjectTypes.get_all()]

    @staticmethod
    def get(name: str) -> Type[Project]:
        for ptype in ProjectTypes.get_all():
            if ptype.TYPE_NAME == name:
                return ptype
        raise KeyError(f"Unknown project type: {name}")

    @staticmethod
    def detect_project_type(directory: str) -> Type[Project]:
        for ptype in ProjectTypes.get_all():
            if ptype.is_project_of_type(directory):
                return ptype
        raise IOError("Unable to detect project type")
