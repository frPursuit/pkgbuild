# Package building tool

This Python utility can be used to easily package projects into standard Linux packages.

## Usage

```
usage: pkgbuild [-h] [-v] [-V] [-r REVISION] [-t {python}] [-o OUTPUT] {deb} [project]

positional arguments:
  {deb}                 The type of package to build
  project               The project to build

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         Increase the output's detail level
  -V, --version         display the program's version
  -r REVISION, --revision REVISION
                        The package's revision. Defaults to 1
  -t {python}, --project-type {python}
                        The project's type
  -o OUTPUT, --output OUTPUT
                        The file to save the package as
```

## Supported project types

*Note: Project types are automatically detected, but can be manually specified using the `--project-type` command line argument.*

### Python

Python projects can be automatically packaged using the `setuptools` framework and `setup.py` script.

## Supported package types

Currently, the only supported package type is `deb`, targetting [Debian](https://www.debian.org/) based systems.

## License

This project is licensed under the [GNU GPLv3 license](./LICENSE).
